create or replace NONEDITIONABLE PACKAGE BODY DOHVAT AS
e_iznimka exception;
------------------------------------------------------------------------------------
--get_predmeti
procedure p_get_predmeti(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
  l_obj JSON_OBJECT_T;
  l_predmeti json_array_t :=JSON_ARRAY_T('[]');
  l_count number;
  l_id number;
  l_string varchar2(1000);
  l_search varchar2(100);
  l_page number; 
  l_perpage number; 
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);
    
    l_string := in_json.TO_STRING; 
    
    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.search'),
        JSON_VALUE(l_string, '$.page' ),
        JSON_VALUE(l_string, '$.perpage' )
    INTO
        l_id,
        l_search,
        l_page,
        l_perpage
    FROM 
       dual;
    
    FOR x IN (
        SELECT 
               json_object('ID' VALUE d.ID, 
                           'PREDMET' VALUE d.NAZIV,
                           'PROFESOR' VALUE p.IME|| ' ' || p.PREZIME,
                           'SMJER' VALUE                           
                               case smjer
                               when 1 then
                                  'komercijalna'
                               when 2 then
                                  'prodavaci'
                               else
                                  'nepoznato'
                               end,
                         'ACTION' VALUE '<i onClick="delPredmet(' || d.ID || ')" class="fa fa-trash delete"/>',
                         'ACTIONN' VALUE '<i onClick="editPredmet('|| d.ID || ')" class="fa-solid fa-pen-clip ispravi"></i>' 
         
                      
               ) as izlaz
             FROM
                teme t,profesori p, predmeti d
             where
                d.deleted=0 and
                t.idprofesora = p.id and
                t.idpredmeta = d.id and
                d.ID = nvl(l_id, d.ID) 
            )
        LOOP
            l_predmeti.append(JSON_OBJECT_T(x.izlaz));
        END LOOP;
        
    SELECT
      count(1)
    INTO
       l_count
    FROM 
       predmeti
    where
        ID = nvl(l_id, ID) ;
        
    l_obj.put('count',l_count);
    l_obj.put('data',l_predmeti);
    out_json := l_obj;
 EXCEPTION
   WHEN OTHERS THEN
       common.p_errlog('p_get_predmeti', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
       l_obj.put('h_message', 'Greška u obradi podataka');
       l_obj.put('h_errcode', 99);
       ROLLBACK;    
    
  END p_get_predmeti;
  

------------------------------------------------------------------------------------
--get_profesori
procedure p_get_profesori(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
  l_obj JSON_OBJECT_T;
  l_profesori json_array_t :=JSON_ARRAY_T('[]');
  l_count number;
  l_id number;
  l_string varchar2(1000);
  l_search varchar2(100);
  l_page number; 
  l_perpage number; 
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);
    
    l_string := in_json.TO_STRING; 
    
    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.search'),
        JSON_VALUE(l_string, '$.page' ),
        JSON_VALUE(l_string, '$.perpage' )
    INTO
        l_id,
        l_search,
        l_page,
        l_perpage
    FROM 
       dual;
    
    FOR x IN (
           SELECT 
               json_object('ID' VALUE t.ID,
                           'PROFESOR' VALUE p.IME || ' ' || p.PREZIME,
                           'EMAIL' VALUE p.EMAIL,
                           'PREDMET' VALUE d.NAZIV,
                           'ACTION' VALUE '<i onClick="delProfesor(' || p.ID || ')" class="fa fa-trash delete"/>',
                           'ACTIONN' VALUE '<i onClick="editProfesor('|| p.ID || ')" class="fa-solid fa-pen-clip ispravi"></i>' 
                            ) as izlaz
             FROM
                teme t, profesori p, predmeti d
             where
                p.deleted=0 and
                t.idprofesora = p.id and
                t.idpredmeta = d.id and
                t.ID = nvl(l_id, t.ID) 
            )
        LOOP
            l_profesori.append(JSON_OBJECT_T(x.izlaz));
        END LOOP;
        
    SELECT
      count(1)
    INTO
       l_count
    FROM 
       profesori
    where
        ID = nvl(l_id, ID) ;
        
    l_obj.put('count',l_count);
    l_obj.put('data',l_profesori);
    out_json := l_obj;
 EXCEPTION
   WHEN OTHERS THEN
       common.p_errlog('p_get_profesori', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
       l_obj.put('h_message', 'Greška u obradi podataka');
       l_obj.put('h_errcode', 99);
       ROLLBACK;    
    
  END p_get_profesori;

------------------------------------------------------------------------------------
-- Dohvat tema
procedure p_get_teme(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
  l_obj JSON_OBJECT_T;
  l_teme json_array_t :=JSON_ARRAY_T('[]');
  l_count number;
  l_id number;
  l_string varchar2(1000);
  l_search varchar2(100);
  l_page number; 
  l_perpage number; 
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);
    
    l_string := in_json.TO_STRING; 
    
    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.search'),
        JSON_VALUE(l_string, '$.page' ),
        JSON_VALUE(l_string, '$.perpage' )
    INTO
        l_id,
        l_search,
        l_page,
        l_perpage
    FROM 
       dual;
    
    FOR x IN (
        SELECT 
               json_object('ID' VALUE d.ID, 
                           'NAZIV' VALUE t.NAZIV,
                           'PROFESOR' VALUE p.IME|| ' ' || p.PREZIME,
                           'PREDMET' VALUE d.NAZIV,
                           'ACTION' VALUE '<i onClick="delTema(' || t.ID || ')" class="fa fa-trash delete"/>',
                           'ACTIONN' VALUE '<i onClick="editTema('|| t.ID || ')" class="fa-solid fa-pen-clip ispravi"></i>', 
                           'ODABIR' VALUE '<button onClick="testt('|| t.ID || ')" id="spremiOdabir" class="unesi"><i class="fa-solid fa-check"></i>  Unesi</button>' 
                           
                            ) as izlaz
             FROM
                teme t,profesori p, predmeti d
             where
                t.deleted=0 and
                t.idprofesora = p.id and
                t.idpredmeta = d.id and
                t.ID = nvl(l_id, t.ID) 
            )
        LOOP
            l_teme.append(JSON_OBJECT_T(x.izlaz));
        END LOOP;
        
    SELECT
      count(1)
    INTO
       l_count
    FROM 
       teme
    where
        ID = nvl(l_id, ID) ;
        
    l_obj.put('count',l_count);
    l_obj.put('data',l_teme);
    out_json := l_obj;
 EXCEPTION
   WHEN OTHERS THEN
       common.p_errlog('p_get_predmeti', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
       l_obj.put('h_message', 'Greška u obradi podataka');
       l_obj.put('h_errcode', 99);
       ROLLBACK;    
    
  END p_get_teme;
------------------------------------------------------------------------------------
--get_ucenici(prodavaci)
procedure p_get_ucenicip(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
  l_obj JSON_OBJECT_T;
  l_ucenici json_array_t :=JSON_ARRAY_T('[]');
  l_count number;
  l_id number;
  l_string varchar2(1000);
  l_search varchar2(100);
  l_page number; 
  l_perpage number; 
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);
    
    l_string := in_json.TO_STRING; 
    
    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.search'),
        JSON_VALUE(l_string, '$.page' ),
        JSON_VALUE(l_string, '$.perpage' )
    INTO
        l_id,
        l_search,
        l_page,
        l_perpage
    FROM 
       dual;
    
    FOR x IN (
           SELECT 
              json_object('ID' VALUE u.ID, 
                           'TEMA' VALUE t.NAZIV,
                           'UCENIK' VALUE u.IME|| ' ' || u.PREZIME,
                           'RAZRED' VALUE r.NAZIV,
                           'MENTOR' VALUE p.IME || ' ' || p.PREZIME,
                           'PREDMET' VALUE pr.NAZIV,
                           'ACTION' VALUE '<i onClick="delUcenikp(' || u.ID || ')" class="fa fa-trash delete"/>',
                           'ACTIONN' VALUE '<i onClick="editUcenikp('|| u.ID || ')" class="fa-solid fa-pen-clip ispravi"></i>' 
                           
                            ) as izlaz
           FROM ucenici  u
              left join odabiri o on
              u.id = o.iducenika 
              left join teme t on
              t.id = o.idteme 
              left join profesori p on
              p.id = t.idprofesora
              left join razredi r on
              u.idrazred = r.id 
              left join predmeti pr on
              pr.id = t.idpredmeta
           WHERE
              u.deleted=0 and
              u.smjer=1 and
              u.ID = nvl(l_id, U.ID)  
            )
        LOOP
            l_ucenici.append(JSON_OBJECT_T(x.izlaz));
        END LOOP;
        
    SELECT
      count(1)
    INTO
       l_count
    FROM 
       ucenici
    where
        ID = nvl(l_id, ID) and
        smjer=1;
        
    l_obj.put('count',l_count);
    l_obj.put('data',l_ucenici);
    out_json := l_obj;
 EXCEPTION
   WHEN OTHERS THEN
       common.p_errlog('p_get_ucenicik', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
       l_obj.put('h_message', 'Greška u obradi podataka');
       l_obj.put('h_errcode', 99);
       ROLLBACK;    
    
  END p_get_ucenicip;
  
------------------------------------------------------------------------------------
--get_ucenici(komer)
procedure p_get_ucenicik(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
  l_obj JSON_OBJECT_T;
  l_ucenici json_array_t :=JSON_ARRAY_T('[]');
  l_count number;
  l_id number;
  l_string varchar2(1000);
  l_search varchar2(100);
  l_page number; 
  l_perpage number; 
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);
    
    l_string := in_json.TO_STRING; 
    
    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.search'),
        JSON_VALUE(l_string, '$.page' ),
        JSON_VALUE(l_string, '$.perpage' )
    INTO
        l_id,
        l_search,
        l_page,
        l_perpage
    FROM 
       dual;
    
    FOR x IN (
           SELECT 
              json_object('ID' VALUE u.ID, 
                           'TEMA' VALUE t.NAZIV,
                           'UCENIK' VALUE u.IME|| ' ' || u.PREZIME,
                           'RAZRED' VALUE r.NAZIV,
                           'MENTOR' VALUE p.IME || ' ' || p.PREZIME,
                           'PREDMET' VALUE pr.NAZIV,
                           'ACTION' VALUE '<i onClick="delUcenikk(' || u.ID || ')" class="fa fa-trash delete"/>',
                           'ACTIONN' VALUE '<i onClick="editUcenikk('|| u.ID || ')" class="fa-solid fa-pen-clip ispravi"></i>' 
                            ) as izlaz
           FROM ucenici  u
              left join odabiri o on
              u.id = o.iducenika 
              left join teme t on
              t.id = o.idteme 
              left join profesori p on
              p.id = t.idprofesora
              left join razredi r on
              u.idrazred = r.id 
              left join predmeti pr on
              pr.id = t.idpredmeta
           WHERE
              u.deleted=0 and
              u.smjer=2 and
              u.ID = nvl(l_id, U.ID)  
            )
        LOOP
            l_ucenici.append(JSON_OBJECT_T(x.izlaz));
        END LOOP;
        
    SELECT
      count(1)
    INTO
       l_count
    FROM 
       ucenici
    where
        ID = nvl(l_id, ID) and
        smjer=2;
        
    l_obj.put('count',l_count);
    l_obj.put('data',l_ucenici);
    out_json := l_obj;
 EXCEPTION
   WHEN OTHERS THEN
       common.p_errlog('p_get_ucenicip', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
       l_obj.put('h_message', 'Greška u obradi podataka');
       l_obj.put('h_errcode', 99);
       ROLLBACK;    
    
  END p_get_ucenicik;
------------------------------------------------------------------------------------
--login
 PROCEDURE p_login(in_json in json_object_t, out_json out json_object_t )AS
    l_obj        json_object_t := json_object_t();
    l_input      VARCHAR2(4000);
    l_record     VARCHAR2(4000);
    l_username   UCENICI.email%TYPE;
    l_password   UCENICI.lozinka%TYPE;
    l_id         UCENICI.id%TYPE;
    l_out        json_array_t := json_array_t('[]');
 BEGIN
    l_obj.put('h_message', '');
    l_obj.put('h_errcode', '');
    l_input := in_json.to_string;
    SELECT
        JSON_VALUE(l_input, '$.username' RETURNING VARCHAR2),
        JSON_VALUE(l_input, '$.password' RETURNING VARCHAR2)
    INTO
        l_username,
        l_password
    FROM
        dual;

    IF (l_username IS NULL OR l_password is NULL) THEN
       l_obj.put('h_message', 'Molimo unesite korisničko ime i zaporku');
       l_obj.put('h_errcod', 101);
       RAISE e_iznimka;
    ELSE
       BEGIN
          SELECT
             id
          INTO 
             l_id
          FROM
             UCENICI
          WHERE
             email = l_username AND 
             lozinka = l_password;
       EXCEPTION
             WHEN no_data_found THEN
                l_obj.put('h_message', 'Nepoznato korisničko ime ili zaporka');
                l_obj.put('h_errcod', 102);
                RAISE e_iznimka;
             WHEN OTHERS THEN
                RAISE;
       END;

       SELECT
          JSON_OBJECT( 
             'ID' VALUE kor.id, 
             'ime' VALUE kor.ime, 
             'prezime' VALUE kor.prezime)
       INTO 
          l_record
       FROM
          UCENICI kor
       WHERE
          id = l_id;

    END IF;

    l_out.append(json_object_t(l_record));
    l_obj.put('data', l_out);
    out_json := l_obj;
 EXCEPTION
    WHEN e_iznimka THEN
       out_json := l_obj; 
    WHEN OTHERS THEN
       common.p_errlog('p_users_upd', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_input);
       l_obj.put('h_message', 'Greška u obradi podataka');
       l_obj.put('h_errcode', 99);
       ROLLBACK;
 END p_login;
 
 

END DOHVAT;