create or replace NONEDITIONABLE PACKAGE BODY OBRISI AS
e_iznimka exception;
-------------------------------------------------------------------------------------------------
--Obriši profesori

  procedure p_obrisi_profesora (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_profesor_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_profesori profesori%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.deleted' )
    INTO
        l_profesori.id,
        l_profesori.deleted
    FROM 
        dual;
  


       begin
          update profesori set
          deleted=1
       where id=l_profesori.id;
          l_message := 'Uspješno ste obrisali profesora!';
          l_errcod  := 100;   
       exception
          when others then
          raise;
       end;



    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_obrisi_profesora;
----------------------------------------------------------------------------------
--Obriši ucenika

  procedure p_obrisi_ucenika (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_predmet_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_ucenici ucenici%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.deleted' )
    INTO
        l_ucenici.id,
        l_ucenici.deleted
    FROM 
        dual;
  


       begin
          update ucenici set
          deleted=1
       where id=l_ucenici.id;
          l_message := 'Uspješno ste obrisali učenika!';
          l_errcod  := 100;   
       exception
          when others then
          raise;
       end;



    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_obrisi_ucenika;
------------------------------------------------------------------------------------
--Obriši predmet

  procedure p_obrisi_predmet (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_predmet_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_predmeti predmeti%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);

 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.deleted' )
    INTO
        l_predmeti.id,
        l_predmeti.deleted
    FROM 
        dual;
  


       begin
          update predmeti set
          deleted=1
       where 
          id=l_predmeti.id;
          l_message := 'Uspješno ste obrisali predmet!';
          l_errcod  := 100;   
       exception
          when others then
          raise;
       end;



    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_obrisi_predmet;
---------------------------------------------------------------------------------
--Obriši odabir

  procedure p_obrisi_odabir (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_predmet_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_odabiri odabiri%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.deleted' )
    INTO
        l_odabiri.id,
        l_odabiri.deleted
    FROM 
        dual;
  


       begin
          update odabiri set
          deleted=1
       where id=l_odabiri.id;
          l_message := 'Uspješno ste obrisali odabir!';
          l_errcod  := 100;   
       exception
          when others then
          raise;
       end;



    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_obrisi_odabir;
----------------------------------------------------------------------------------
--Obriši razred

  procedure p_obrisi_razred (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_profesor_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_razredi razredi%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
     l_temp number;
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.deleted' )
    INTO
        l_razredi.id,
        l_razredi.deleted
    FROM 
        dual;
  


       begin
          update razredi set
          deleted=1
       where id=l_razredi.id;
          l_message := 'Uspješno ste obrisali razred!';
          l_errcod  := 100;   
       exception
          when others then
          raise;
       end;



    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_obrisi_razred;
-------------------------------------------------------------------------------------
--Obriši temu

  procedure p_obrisi_temu (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_tema_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_teme teme%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.deleted' )
    INTO
        l_teme.id,
        l_teme.deleted
    FROM 
        dual;
  


       begin
          update teme set
          deleted=1
       where id=l_teme.id;
          l_message := 'Uspješno ste obrisali temu!';
          l_errcod  := 100;   
       exception
          when others then
          raise;
       end;



    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_obrisi_temu;
------------------------------------------------------------------------------------
END OBRISI;