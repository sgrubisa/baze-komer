create or replace NONEDITIONABLE PACKAGE BODY SPREMI AS
e_iznimka exception;
-------------------------------------------------------------------------------------------------
--Spremi profesori

  procedure p_save_profesori (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_profesor_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_profesori profesori%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
     l_temp number;
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.ime'),
        JSON_VALUE(l_string, '$.prezime' ),
        JSON_VALUE(l_string, '$.email' ),
        JSON_VALUE(l_string, '$.lozinka' )
    INTO
        l_profesori.id,
        l_profesori.ime,
        l_profesori.prezime,
        l_profesori.email,
        l_profesori.lozinka
    FROM 
        dual;
  


       begin
       if nvl(l_profesori.id,0)=0 then
         insert into profesori(ime, prezime, email, lozinka) values (
                             l_profesori.ime, 
                             l_profesori.prezime, 
                             l_profesori.email, 
                             l_profesori.lozinka);
         l_message := 'Uspješno ste unijeli profesora!';
         l_errcod := 100;
       else
        update profesori 
        set
        ime =l_profesori.ime,
        prezime=l_profesori.prezime,
        email=l_profesori.email,
        lozinka=l_profesori.lozinka
        where ID=l_profesori.ID;
        
        l_message := 'Uspješno ste izmijenili profesora!';
        l_errcod  := 100;            
      end if;
       exception
          when others then
          raise;
       end;



    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_save_profesori;
-------------------------------------------------------------------------------------------------
--Spremi ucenici

  procedure p_save_ucenici (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_ucenici_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_ucenici ucenici%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
     l_temp number;
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

   
    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.IDrazred' ),
        JSON_VALUE(l_string, '$.ime'),
        JSON_VALUE(l_string, '$.prezime' ),
        JSON_VALUE(l_string, '$.email' ),
        JSON_VALUE(l_string, '$.lozinka' ),
        JSON_VALUE(l_string, '$.smjer' )
    INTO
        l_ucenici.id,
        l_ucenici.IDrazred,
        l_ucenici.ime,
        l_ucenici.prezime,
        l_ucenici.email,
        l_ucenici.lozinka,
        l_ucenici.smjer
    FROM 
        dual;

   
      begin
       if nvl(l_ucenici.id,0)=0 then
         insert into ucenici(IDrazred,ime,prezime, email, lozinka, smjer) values (
                                l_ucenici.IDrazred,
                                l_ucenici.ime,
                                l_ucenici.prezime,
                                l_ucenici.email,
                                l_ucenici.lozinka,
                                l_ucenici.smjer);
         l_message := 'Uspješno ste unijeli ucenika!';
         l_errcod := 100;
        else
            update ucenici
               set
                  ime =l_ucenici.ime,
                  prezime=l_ucenici.prezime,
                  email=l_ucenici.email,
                  lozinka=l_ucenici.lozinka,
                  smjer=l_ucenici.smjer
               where ID=l_ucenici.ID;
        l_message := 'Uspješno ste izmijenili učenika!';
        l_errcod  := 100;            
      end if;
         
   exception
         when others then
         raise;
      end;

   
   
    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_save_ucenici;
------------------------------------------------------------------------------------
--spremi predmet
procedure p_save_predmeti (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_ucenici_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_predmeti predmeti%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
     l_temp number;
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.naziv' ),
        JSON_VALUE(l_string, '$.smjer' )
    INTO
        l_predmeti.id,
        l_predmeti.naziv,
        l_predmeti.smjer
    FROM 
        dual;



       begin
         if nvl(l_predmeti.id,0)=0 then
         insert into predmeti(naziv, smjer) values (
                                l_predmeti.naziv,
                                l_predmeti.smjer);
         l_message := 'Uspješno ste unijeli predmet!';
         l_errcod := 100;
        else
            update predmeti set
                naziv=l_predmeti.naziv,
                smjer=l_predmeti.smjer
                where id=l_predmeti.id;
            l_message := 'Uspješno ste izmjenili predmet!';
            l_errcod := 100;
        end if;
       exception
          when others then
          raise;
       end;



    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_save_predmeti;
----------------------------------------------------------------------------------
--Spremi Odabir
procedure p_save_odabir (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_ucenici_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_odabiri odabiri%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
     l_temp number;
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.IDteme' ),
        JSON_VALUE(l_string, '$.UserID' )
    INTO
        l_odabiri.id,
        l_odabiri.IDteme,
        l_odabiri.IDucenika
    FROM 
        dual;
        
     common.p_logiraj('test2104','ID:' || l_odabiri.id || 'IDteme:' || l_odabiri.IDteme || 'IDucenika:' || l_odabiri.IDucenika );   

       begin
         if nvl(l_odabiri.id,0)=0 then
         insert into odabiri(IDteme, IDucenika) values (
                                l_odabiri.IDteme,
                                l_odabiri.IDucenika);
         l_message := 'Uspješno ste odabrali temu!';
         l_errcod := 100;
        else
            update odabiri set
                IDteme=l_odabiri.IDteme,
                IDucenika=l_odabiri.IDucenika
                where id=l_odabiri.id;
            l_message := 'Uspješno ste izmjenili predmet!';
            l_errcod := 100;
        end if;
       exception
          when others then
          raise;
       end;


    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       common.p_errlog('p_save_odabir', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       l_obj.put('h_message',l_message);
       l_obj.put('h_errcod',l_errcod);
       out_json := l_obj;
       ROLLBACK;    
END p_save_odabir;
-------------------------------------------------------------------------------------------------
--Spremi temu

  procedure p_save_temu (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_teme_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_teme teme%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
     l_temp number;
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.IDpredmeta'),
        JSON_VALUE(l_string, '$.IDprofesora' ),
        JSON_VALUE(l_string, '$.IDrazred' ),
        JSON_VALUE(l_string, '$.naziv' )
    INTO
        l_teme.id,
        l_teme.IDpredmeta,
        l_teme.IDprofesora,
        l_teme.IDrazred,
        l_teme.naziv
    FROM 
        dual;
  


       begin
       if nvl(l_teme.id,0)=0 then
         insert into teme(IDpredmeta, IDprofesora, IDrazred, naziv) values (
                             l_teme.IDpredmeta, 
                             l_teme.IDprofesora, 
                             l_teme.IDrazred,
                             l_teme.naziv);
         l_message := 'Uspješno ste unijeli temu!';
         l_errcod := 100;
       else
        update teme 
        set
        IDpredmeta =l_teme.IDpredmeta,
        IDprofesora=l_teme.IDprofesora,
        IDrazred=l_teme.IDrazred,
        naziv=l_teme.naziv
        where ID=l_teme.ID;
        
        l_message := 'Uspješno ste izmijenili temu!';
        l_errcod  := 100;            
      end if;
       exception
          when others then
          raise;
       end;



    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_save_temu;
---------------------------------------------------------------------------------
--Spremanje za prvu stranicu na frontendu za učenike

  procedure p_save_uceniciweb (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
     l_obj JSON_OBJECT_T;
     l_teme_arr json_array_t :=JSON_ARRAY_T('[]');
     l_count number;
     l_string varchar2(1000);
     l_predmeti predmeti%rowtype;
     l_teme teme%rowtype;
     l_ucenici ucenici%rowtype;
     l_razredi razredi%rowtype;
     l_profesori profesori%rowtype;
     l_message varchar2(1000);
     l_errcod int(3);
     l_temp number;
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);

    l_string := in_json.TO_STRING; 

    SELECT
        JSON_VALUE(l_string, '$.IDt' ),
        JSON_VALUE(l_string, '$.IDu' ),
        JSON_VALUE(l_string, '$.IDr' ),
        JSON_VALUE(l_string, '$.IDpr' ),
        JSON_VALUE(l_string, '$.IDp' ),
        JSON_VALUE(l_string, '$.naziv'),
        JSON_VALUE(l_string, '$.ime' ),
        JSON_VALUE(l_string, '$.prezime' ),
        JSON_VALUE(l_string, '$.razred' ),
        JSON_VALUE(l_string, '$.imeP' ),
        JSON_VALUE(l_string, '$.prezimeP' ),
        JSON_VALUE(l_string, '$.nazivPr' )
    INTO
        l_teme.id,
        l_ucenici.id,
        l_razredi.id,
        l_predmeti.id,
        l_profesori.id,
        l_teme.naziv,
        l_ucenici.ime,
        l_ucenici.prezime,
        l_razredi.naziv,
        l_profesori.ime,
        l_profesori.prezime,
        l_predmeti.naziv
    FROM 
        dual;
  


       begin
            begin
                update teme 
                set
                naziv=l_teme.naziv
                where ID=l_teme.id;
            end;
            begin
                update ucenici
                set
                ime=l_ucenici.ime,
                prezime=l_ucenici.prezime
                where ID=l_ucenici.id;
            end;
            begin
                update razredi
                set
                naziv=l_Razredi.naziv
                where ID=l_razredi.id;
            end;
            begin
                update predmeti
                set
                naziv=l_predmeti.naziv
                where ID=l_predmeti.id;
            end;
            begin
                update profesori
                set
                ime=l_profesori.ime,
                prezime=l_profesori.prezime
                where ID=l_profesori.id;
            end;
        l_message := 'Uspješno ste izmijenili podatke!';
        l_errcod  := 100;            
      
       exception
          when others then
          raise;
       end;



    l_obj.put('h_message',l_message);
    l_obj.put('h_errcod',l_errcod);
    out_json := l_obj;
    commit;
 EXCEPTION
    when e_iznimka then
        l_obj.put('h_message',l_message);
        l_obj.put('h_errcod',l_errcod);
        out_json := l_obj;
        rollback;
   WHEN OTHERS THEN
       common.p_errlog('p_save_uceniciweb', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
       l_message:='Doslo je do pogreske u sustavu. Molimo pokusajte ponovno';
       l_errcod:=999;
       ROLLBACK;    
END p_save_uceniciweb;

END SPREMI;