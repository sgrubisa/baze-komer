--drop tablica
drop table odabiri;
drop table ucenici;
drop table teme;
drop table profesori;
drop table predmeti;
drop table razredi;


-- drop sequencea
drop sequence odabiri_id_Seq;
drop sequence predmeti_id_Seq;
drop sequence profesori_id_Seq;
drop sequence teme_id_Seq;
drop sequence ucenici_id_Seq;
drop sequence razredi_id_Seq;

--tablica ucenici--
CREATE TABLE Ucenici (
	ID NUMBER(9, 0) NOT NULL,
	IDrazred NUMBER(9, 0) NOT NULL,
	ime VARCHAR2(30) NOT NULL,
	prezime VARCHAR2(30) NOT NULL,
	email VARCHAR2(30) UNIQUE NOT NULL,
	lozinka VARCHAR2(20) NOT NULL,
	smjer NUMBER(1, 0) NOT NULL,
	deleted NUMBER(1, 0) default 0 NOT NULL,
	created TIMESTAMP(6) NOT NULL,
	updated TIMESTAMP(6),
	constraint UCENICI_PK PRIMARY KEY (ID));

CREATE sequence UCENICI_ID_SEQ;

CREATE trigger BI_UCENICI_ID
  before insert on Ucenici
  for each row
begin
  select UCENICI_ID_SEQ.nextval into :NEW.ID from dual;
  select systimestamp into :NEW.created from dual;
end;
/

CREATE trigger BU_UCENICI_UPD
  before update on Ucenici
  for each row
begin
  select systimestamp into :NEW.updated from dual;
end;
/

--tablica s temama--
CREATE TABLE Teme (
	ID NUMBER(9, 0) NOT NULL,
	IDpredmeta NUMBER(9, 0) NOT NULL,
	IDprofesora NUMBER(9, 0) NOT NULL,
	IDrazred NUMBER(9, 0) NOT NULL,
	naziv VARCHAR2(200) NOT NULL,
	deleted NUMBER(1, 0) default 0 NOT NULL,
	created TIMESTAMP(6) NOT NULL,
	updated TIMESTAMP(6),
	constraint TEME_PK PRIMARY KEY (ID));

CREATE sequence TEME_ID_SEQ;

CREATE trigger BI_TEME_ID
  before insert on Teme
  for each row
begin
  select TEME_ID_SEQ.nextval into :NEW.ID from dual;
  select systimestamp into :NEW.created from dual;
end;
/

CREATE trigger BU_TEME_UPD
  before update on Teme
  for each row
begin
  select systimestamp into :NEW.updated from dual;
end;
/

--tablica nakon sto u?enik napravi odabir--
CREATE TABLE Odabiri (
	ID NUMBER(9, 0) NOT NULL,
	IDteme NUMBER(9, 0) UNIQUE NOT NULL,
	IDucenika NUMBER(9, 0) UNIQUE NOT NULL,
	deleted NUMBER(1, 0) default 0 NOT NULL,
	created TIMESTAMP(6) NOT NULL,
	updated TIMESTAMP(6),
	constraint ODABIRI_PK PRIMARY KEY (ID));

CREATE sequence ODABIRI_ID_SEQ;

CREATE trigger BI_ODABIRI_ID
  before insert on Odabiri
  for each row
begin
  select ODABIRI_ID_SEQ.nextval into :NEW.ID from dual;
  select systimestamp into :NEW.created from dual;
end;
/

CREATE trigger BU_ODABIRI_UPD
  before update on Odabiri
  for each row
begin
  select systimestamp into :NEW.updated from dual;
end;
/

--tablica profesora--
CREATE TABLE Profesori (
	ID NUMBER(9, 0) NOT NULL,
	ime VARCHAR2(30) NOT NULL,
	prezime VARCHAR2(30) NOT NULL,
	email VARCHAR2(30) UNIQUE NOT NULL,
	lozinka VARCHAR2(20) NOT NULL,
	deleted NUMBER(1, 0) default 0 NOT NULL,
	created TIMESTAMP(6) NOT NULL,
	updated TIMESTAMP(6),
	constraint PROFESORI_PK PRIMARY KEY (ID));

CREATE sequence PROFESORI_ID_SEQ;

CREATE trigger BI_PROFESORI_ID
  before insert on Profesori
  for each row
begin
  select PROFESORI_ID_SEQ.nextval into :NEW.ID from dual;
  select systimestamp into :NEW.created from dual;
end;
/

CREATE trigger BU_PROFESORI_UPD
  before update on Profesori
  for each row
begin
  select systimestamp into :NEW.updated from dual;
end;
/

--tablica s predmetima--
CREATE TABLE Predmeti (
	ID INT NOT NULL,
	naziv VARCHAR2(100) NOT NULL,
	smjer NUMBER(1, 0) NOT NULL,
	deleted NUMBER(1, 0) default 0 NOT NULL,
	created TIMESTAMP(6) NOT NULL,
	updated TIMESTAMP(6),
	constraint PREDMETI_PK PRIMARY KEY (ID));

CREATE sequence PREDMETI_ID_SEQ;

CREATE trigger BI_PREDMETI_ID
  before insert on Predmeti
  for each row
begin
  select PREDMETI_ID_SEQ.nextval into :NEW.ID from dual;
  select systimestamp into :NEW.created from dual;
end;
/

CREATE trigger BU_PREDMETI_UPD
  before update on Predmeti
  for each row
begin
  select systimestamp into :NEW.updated from dual;
end;
/

--tablica s razredima--
CREATE TABLE razredi (
	ID NUMBER(9, 0) NOT NULL,
	naziv VARCHAR2(30) NOT NULL,
    deleted NUMBER(1, 0) default 0 NOT NULL,
	created TIMESTAMP(6) NOT NULL,
	updated TIMESTAMP(6),
	constraint RAZREDI_PK PRIMARY KEY (ID));

CREATE sequence RAZREDI_ID_SEQ;

CREATE trigger BI_RAZREDI_ID
  before insert on razredi
  for each row
begin
  select RAZREDI_ID_SEQ.nextval into :NEW.ID from dual;
  select systimestamp into :NEW.created from dual;
end;
/

CREATE trigger BU_RAZREDI_UPD
  before update on Razredi
  for each row
begin
  select systimestamp into :NEW.updated from dual;
end;
/
ALTER TABLE Ucenici ADD CONSTRAINT Ucenici_fk0 FOREIGN KEY (IDrazred) REFERENCES razredi(ID);

ALTER TABLE Teme ADD CONSTRAINT Teme_fk0 FOREIGN KEY (IDpredmeta) REFERENCES Predmeti(ID);
ALTER TABLE Teme ADD CONSTRAINT Teme_fk1 FOREIGN KEY (IDprofesora) REFERENCES Profesori(ID);
ALTER TABLE Teme ADD CONSTRAINT Teme_fk2 FOREIGN KEY (IDrazred) REFERENCES razredi(ID);

ALTER TABLE Odabiri ADD CONSTRAINT Odabiri_fk0 FOREIGN KEY (IDteme) REFERENCES Teme(ID);
ALTER TABLE Odabiri ADD CONSTRAINT Odabiri_fk1 FOREIGN KEY (IDucenika) REFERENCES Ucenici(ID)
ALTER TABLE Odabiri ADD CONSTRAINT odabiri_uk UNIQUE (IDteme, IDucenika);

--inserti u talbice
INSERT INTO PROFESORI (ID, IME, PREZIME, EMAIL, LOZINKA) VALUES (1, 'Ivana', 'Ivi?', 'iivic@vub.hr', '123124');
INSERT INTO PROFESORI (ID, IME, PREZIME, EMAIL, LOZINKA) VALUES (2, 'Marinko', 'Mari?', 'mmaric@vub.hr', '34534fd');
INSERT INTO PROFESORI (ID, IME, PREZIME, EMAIL, LOZINKA) VALUES (3, 'Alen', 'Alenic', 'aalenic@vub.hr', '234325f');
INSERT INTO PROFESORI (ID, IME, PREZIME, EMAIL, LOZINKA) VALUES (4, 'Stevo', 'Stevic', 'sstevic@vub.hr', 'sfdsfsd');
INSERT INTO PROFESORI (ID, IME, PREZIME, EMAIL, LOZINKA) VALUES (5, 'Marina', 'Mira', 'mmira@vub.hr', '3efs');
INSERT INTO PROFESORI (ID, IME, PREZIME, EMAIL, LOZINKA) VALUES (6, 'Filip', 'Filipcic', 'ffilip@vub.hr', '6468zh');
INSERT INTO PROFESORI (ID, IME, PREZIME, EMAIL, LOZINKA) VALUES (7, 'Mateo', 'Marulic', 'mmarulic@vub.hr', '7954zd');
INSERT INTO PROFESORI (ID, IME, PREZIME, EMAIL, LOZINKA) VALUES (8, 'Matija', 'Mati?', 'mmatic@vub.hr', 'r6zre');
INSERT INTO PROFESORI (ID, IME, PREZIME, EMAIL, LOZINKA) VALUES (9, 'Milunka', 'Mili?', 'mmil@vub.hr', '845g');
INSERT INTO PROFESORI (ID, IME, PREZIME, EMAIL, LOZINKA) VALUES (10, 'Andrej', 'Andric','aandric@vub.hr', '9856zut');


INSERT INTO razredi (ID, naziv) VALUES (1, '3.a'); 
INSERT INTO razredi (ID, naziv) VALUES (2, '3.b'); 
INSERT INTO razredi (ID, naziv) VALUES (3, '3.c'); 
INSERT INTO razredi (ID, naziv) VALUES (4, '4.a'); 
INSERT INTO razredi (ID, naziv) VALUES (5, '4.b'); 
INSERT INTO razredi (ID, naziv) VALUES (6, '4.c'); 


-- 1 Prodava?i
-- 2 Komercijalisti

INSERT INTO Ucenici (ID, IDrazred, ime, prezime, email, lozinka, smjer) VALUES (1, 2, 'Sebastian', 'Grubisa', 'sgrubisa@vub.hr', '13123', 1);
INSERT INTO Ucenici (ID, IDrazred, ime, prezime, email, lozinka, smjer) VALUES (2, 4, 'Alen', 'Kreuzer', 'akreuzer@vub.hr', 'fsdf,', 2);
INSERT INTO Ucenici (ID, IDrazred, ime, prezime, email, lozinka, smjer) VALUES (3, 2, 'Marko', 'Mari', 'mmm@vub.hr', 'sdfsdf', 1);
INSERT INTO Ucenici (ID, IDrazred, ime, prezime, email, lozinka, smjer) VALUES (4, 6, 'Maja', 'Marisa', 'mmarisa@vub.hr', 'fsdfsdf', 2);
INSERT INTO Ucenici (ID, IDrazred, ime, prezime, email, lozinka, smjer) VALUES (5, 1, 'Darko', 'DArki?', 'darki@vub.hr', '321312', 1);

INSERT INTO Predmeti (ID, naziv, smjer) VALUES (1, 'Predmet prodavaca',1);
INSERT INTO Predmeti (ID, naziv, smjer) VALUES (2, 'Prodaja',1);
INSERT INTO Predmeti (ID, naziv, smjer) VALUES (3, 'Knjigovodstvo',2);
INSERT INTO Predmeti (ID, naziv, smjer) VALUES (4, 'Racuni',2);
INSERT INTO Predmeti (ID, naziv, smjer) VALUES (5, 'Odnos s kupcima',1);


INSERT INTO Teme (ID, IDpredmeta, IDprofesora, IDrazred, naziv) VALUES (1, 1, 10, 1, 'Prodaja u samoposlugi');
INSERT INTO Teme (ID, IDpredmeta, IDprofesora, IDrazred, naziv) VALUES (2, 2, 2, 2, 'Osnivanje trgovine');
INSERT INTO Teme (ID, IDpredmeta, IDprofesora, IDrazred, naziv) VALUES (3, 3, 4, 4, 'Organizacija racuna');
INSERT INTO Teme (ID, IDpredmeta, IDprofesora, IDrazred, naziv) VALUES (4, 4, 3, 6, 'Knjizenje');

INSERT INTO Odabiri (ID, IDteme, IDucenika) VALUES (1, 1, 1);
INSERT INTO Odabiri (ID, IDteme, IDucenika) VALUES (2, 4, 2);
INSERT INTO Odabiri (ID, IDteme, IDucenika) VALUES (3, 3, 2);
INSERT INTO Odabiri (ID, IDteme, IDucenika) VALUES (4, 2, 3);


--karakteristi?ne SELECTOVE
--paketi koji se nalze na gitu, a treba ih ubaciti u Packages
--prebaciti router, dohvat i sl. prosla predavanja, git 
--dohvat, filter, spremi - tako nekako




