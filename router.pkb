create or replace NONEDITIONABLE PACKAGE BODY ROUTER AS

  procedure p_main(p_in in varchar2, p_out out varchar2) AS
    l_obj JSON_OBJECT_T;
    l_procedura varchar2(40);
  BEGIN
    l_obj := JSON_OBJECT_T(p_in);

    SELECT
        JSON_VALUE(p_in, '$.procedura' RETURNING VARCHAR2)
    INTO
        l_procedura
    FROM DUAL;

    CASE l_procedura
    WHEN 'p_get_profesori' THEN
        dohvat.p_get_profesori(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_login' THEN
        null;
        dohvat.p_login(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_get_predmeti' THEN
        dohvat.p_get_predmeti(JSON_OBJECT_T(p_in), l_obj); 
    WHEN 'p_get_teme' THEN
        dohvat.p_get_teme(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_get_ucenicik' THEN
        dohvat.p_get_ucenicik(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_get_ucenicip' THEN
        dohvat.p_get_ucenicip(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_save_profesori' THEN
        spremi.p_save_profesori(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_save_ucenici' THEN
        spremi.p_save_ucenici(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_save_predmeti' THEN
        spremi.p_save_predmeti(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_save_ucenici' THEN
        spremi.p_save_ucenici(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_save_odabir' THEN
        spremi.p_save_odabir(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_save_temu' THEN
        spremi.p_save_temu(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_save_uceniciweb' THEN
        spremi.p_save_uceniciweb(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_obrisi_profesora' THEN
        obrisi.p_obrisi_profesora(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_obrisi_ucenika' THEN
        obrisi.p_obrisi_ucenika(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_obrisi_odabir' THEN
        obrisi.p_obrisi_odabir(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_obrisi_razred' THEN
        obrisi.p_obrisi_razred(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_obrisi_predmet' THEN
        obrisi.p_obrisi_predmet(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_obrisi_temu' THEN
        obrisi.p_obrisi_temu(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_check_predmeti' THEN
        filter.p_check_predmeti(JSON_OBJECT_T(p_in), l_obj);
        
        
  

    ELSE
        l_obj.put('h_message', ' Nepoznata metoda ' || l_procedura);
        l_obj.put('h_errcode', 997);
    END CASE;
    p_out := l_obj.TO_STRING;
  END p_main;
END ROUTER;