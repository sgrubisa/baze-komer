create or replace NONEDITIONABLE PACKAGE SPREMI AS 

  procedure p_save_profesori(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_save_ucenici(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_save_predmeti (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_save_odabir (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_save_temu (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_save_uceniciweb (in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);


END SPREMI;