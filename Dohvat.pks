create or replace NONEDITIONABLE PACKAGE DOHVAT AS 

  procedure p_get_profesori(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_login(in_json in json_object_t, out_json out json_object_t);
  procedure p_get_predmeti(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_get_ucenicik(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_get_ucenicip(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_get_teme(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
 

END DOHVAT;