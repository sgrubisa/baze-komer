create or replace NONEDITIONABLE PACKAGE OBRISI AS 

  procedure p_obrisi_profesora(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_obrisi_odabir(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_obrisi_predmet(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_obrisi_ucenika(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_obrisi_razred(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_obrisi_temu(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);

END OBRISI;